############################################################
#       _  _____ _____ _____ _   _ _____ ___ ___  _   _    #
#      / \|_   _|_   _| ____| \ | |_   _|_ _/ _ \| \ | |   #
#     / _ \ | |   | | |  _| |  \| | | |  | | | | |  \| |   #
#    / ___ \| |   | | | |___| |\  | | |  | | |_| | |\  |   #
#   /_/   \_\_|   |_| |_____|_| \_| |_| |___\___/|_| \_|   #
#                                                          #
#         CE FICHIER FAIT PARTIE DU TEMPLATE ZORVAL        #
#           IL EST VERSIONNÉ DANS UN DÉPÔT GIT             #
#              MERCI DE DE PAS LE MODIFIER                 #
#                                                          #
############################################################

export NORMAL=$(echo -e '\033[00;00m')
export CYAN=$(echo -e '\033[01;36m')

HISTCONTROL=ignoredups
HISTSIZE=999999
HISTFILESIZE=10485760 # 10 Mo

export HISTTIMEFORMAT="[${CYAN}%F %T${NORMAL}] "


# vim:ft=sh
