############################################################
#       _  _____ _____ _____ _   _ _____ ___ ___  _   _    #
#      / \|_   _|_   _| ____| \ | |_   _|_ _/ _ \| \ | |   #
#     / _ \ | |   | | |  _| |  \| | | |  | | | | |  \| |   #
#    / ___ \| |   | | | |___| |\  | | |  | | |_| | |\  |   #
#   /_/   \_\_|   |_| |_____|_| \_| |_| |___\___/|_| \_|   #
#                                                          #
#         CE FICHIER FAIT PARTIE DU TEMPLATE ZORVAL        #
#           IL EST VERSIONNÉ DANS UN DÉPÔT GIT             #
#              MERCI DE DE PAS LE MODIFIER                 #
#                                                          #
############################################################

# Copyright © 2019 Alban Vidal - alban.vidal@gendarmerie.interieur.gouv.fr

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

#
# Utilité : Fonction permettant de rechercher les dernières connexions ssh au travers des différents fichiers /var/log/wtmp
#
# Arguments :
# - $1 (optionnel) => si spécifié, permet de définir le nombre de dernières connexions, sinon affiche les 10 dernières

function zorval_last() {

    # Test si la personne a donné un nombre, et seulement un nombre
    if echo "$1"|grep -q -E '^[0-9]+$' ; then
        NOMBRE=$1
    else
        # Sinon par défaut on affiche les 10 derniers
        NOMBRE=10
    fi

    # Recherche tous les fichiers wtmp dans /var/log/
    # Note : on fait le trie par ordre de date, du plus récent au plus vieux
    ALL_WTMP=$(ls -tl /var/log/wtmp*|awk '{print $9}')

    # Pour chaque fichier, on récupère ceux qui se sont connectés
    LAST=$( \
        for W in $ALL_WTMP ; do
            # Option -w, --fullnames      => Afficher les noms d'utilisateur et de domaine complets sur la sortie.
            # Option -f, --file <fichier> => Indiquer à last d’utiliser le fichier  indiqué  au  lieu  de  /var/log/wtmp.
            last -w -f "$W" \
            |grep pts       \
            |tr -s " "
        done \
    )

    # Puis on récupère les 10 derniers
    echo "$LAST"|head -n $NOMBRE | column -t
}
