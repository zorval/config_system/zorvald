#!/usr/bin/env bash

############################################################
#       _  _____ _____ _____ _   _ _____ ___ ___  _   _    #
#      / \|_   _|_   _| ____| \ | |_   _|_ _/ _ \| \ | |   #
#     / _ \ | |   | | |  _| |  \| | | |  | | | | |  \| |   #
#    / ___ \| |   | | | |___| |\  | | |  | | |_| | |\  |   #
#   /_/   \_\_|   |_| |_____|_| \_| |_| |___\___/|_| \_|   #
#                                                          #
#         CE FICHIER FAIT PARTIE DU TEMPLATE ZORVAL        #
#           IL EST VERSIONNÉ DANS UN DÉPÔT GIT             #
#              MERCI DE DE PAS LE MODIFIER                 #
#                                                          #
############################################################

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias ls='ls --color=auto'
alias ll='ls --color=auto -alF'
alias la='ls --color=auto -A'
alias l='ls --color=auto -CF'

alias ipa='ip -color -brief address | sort | column -t'
alias journalctl='journalctl -o short-iso'
alias jf='journalctl -f'
alias gg='cd /srv/git'
alias vi='vim'
alias check_conf='sudo /usr/local/sbin/zorval/check_conf'
