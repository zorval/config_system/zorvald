profile_info_system() {

    source /etc/os-release

    case $1 in

        '--uptime')
            BOOT_TIME=$(awk '/^btime/ {print $2}' /proc/stat)
            NOW=$(date "+%s")
            echo -n "$(date --date=@$BOOT_TIME '+%A %x %T%z') "
            eval "echo $(date --utc --date="@$(($NOW - $BOOT_TIME))" '+\($((%s/3600/24))j %Hh %Mm\)')"
            ;;

        '--release')
            if [ $ID = "rhel" ]; then
                echo $PRETTY_NAME
            elif [ $ID = "debian" ]; then
                if [ -f /etc/debian_version ]; then
                    echo "$PRETTY_NAME $(cat /etc/debian_version)"
                else
                    echo "$PRETTY_NAME"
                fi
            elif [ $ID = "ubuntu" ]; then
                echo "$PRETTY_NAME"
            fi
            ;;

        '--procs')
            echo $(( $(ps --no-headers -e | wc --lines) - 1 ))
            ;;

        '--kernel')
            uname --kernel-name --kernel-release --machine
            ;;

        '--users')
            users | wc --words
            ;;

    esac
}

profile_check_memory() {

    mem_avail=$(awk '/^MemAvailable/ {print $2}' /proc/meminfo)
    mem_total=$(awk '/^MemTotal/ {print $2}' /proc/meminfo)
    mem_not_avail=$(( $mem_total - $mem_avail ))
    percent_mem_used=$(echo "$mem_not_avail / $mem_total * 100" | bc -l)
    warn=$(echo "$mem_total * 0.70" | bc -l)
    crit=$(echo "$mem_total * 0.90" | bc -l)

    if (( $(echo "$mem_not_avail > $crit" | bc -l) )); then
        COLOR=$ROUGE
    elif (( $(echo "$mem_not_avail > $warn" | bc -l) )); then
        COLOR=$JAUNE
    else
        COLOR=$VERT
    fi
    LANG=C printf "${COLOR}%2.1f%%${NORMAL} (%s/%s)"                    \
        "$percent_mem_used"                                             \
        "$(numfmt --to=iec --suffix=B --from-unit=1024 $mem_not_avail)" \
        "$(numfmt --to=iec --suffix=B --from-unit=1024 $mem_total)"
}
profile_check_swap() {

    swap_total=$(awk '/^SwapTotal/ {print $2}' /proc/meminfo)
    if (( $swap_total > 0 )); then
        swap_free=$(awk '/^SwapFree/ {print $2}' /proc/meminfo)
        swap_used=$(( $swap_total - $swap_free ))
        swap_percent=$(echo "$swap_used / $swap_total * 100" | bc -l)
    else
        swap_used=0
        swap_percent=0.0
    fi
    warn=$(echo "$swap_total * 0.20" | bc -l)
    crit=$(echo "$swap_total * 0.50" | bc -l)
    if (( $(echo "$swap_used > $crit" | bc -l) )); then
        COLOR=$ROUGE
    elif (( $(echo "$swap_used > $warn" | bc -l) )); then
        COLOR=$JAUNE
    else
        COLOR=$VERT
    fi
    LANG=C printf "${COLOR}%2.1f%%${NORMAL} (%s/%s)"                    \
        "$swap_percent"                                             \
        "$(numfmt --to=iec --suffix=B --from-unit=1024 $swap_used)" \
        "$(numfmt --to=iec --suffix=B --from-unit=1024 $swap_total)"
}
profile_check_load() {

    cpus=$(grep -c ^processor /proc/cpuinfo)
    load15=$(awk '{print $3}' /proc/loadavg)
    warn=$(echo "$cpus * 0.50" | bc -l)
    crit=$(echo "$cpus * 0.80" | bc -l)
    if (( $(echo "$load15 > $crit" | bc -l) )); then
        COLOR=$ROUGE
    elif (( $(echo "$load15 > $warn" | bc -l) )); then
        COLOR=$JAUNE
    else
        COLOR=$VERT
    fi
    echo -ne "${COLOR}${load15}${NORMAL} pour $cpus CPU"
}

test_systemd() {

    SYSTEMCTL_OK="0 loaded units listed."
    SYSTEMCTL_STATE=$(systemctl --failed)
    if grep "$SYSTEMCTL_OK" <(echo "$SYSTEMCTL_STATE") >/dev/null; then
       echo -e ${VERT}"OK"${NORMAL}
    else
       echo -e ${ROUGE}"FAILED"${NORMAL}
    fi

}


echo -e "
${GRAS}${SSLIGNE}${LBLEU}Système${NORMAL}

- OS               : $(profile_info_system --release)
- Noyau            : $(profile_info_system --kernel)
- Uptime           : $(profile_info_system --uptime)
- Charge           : $(/usr/local/bin/zorval/check_load --print)
- Util. mémoire    : $(profile_check_memory)
- Util. Swap       : $(profile_check_swap)
- Statut systemd   : $(test_systemd)
- Nb. de processus : $(profile_info_system --procs)
- Utilisateur(s)   : $(profile_info_system --users)
"
echo
