if [ "$EUID" -eq 0 ] || sudo -n -l /usr/local/sbin/zorval/check_conf >/dev/null 2>&1; then
    export PYTHONIOENCODING="utf8"
    echo -ne "${GRAS}${SSLIGNE}${LBLEU}Conformité${NORMAL} "
    echo -n "(check_conf) : "
    TXT=$(timeout 2s sudo /usr/local/sbin/zorval/check_conf)
    RC=$?
    unset PYTHONIOENCODING
    if [ "$RC" -eq 124 ]; then
        echo -e "${GRAS}${ROUGE}Erreur : timeout${NORMAL}"
    else
        if [ $RC -eq 0 ]; then
           printf "$VERT"
        else
           printf "$ROUGE"
        fi
        echo "$TXT" | head -n 1
        echo -e "${NORMAL}"
    fi
fi

