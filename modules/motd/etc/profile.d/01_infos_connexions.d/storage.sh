#echo -e "
#${GRAS}${SSLIGNE}${LBLEU}Volumétrie${NORMAL}
#"
if timeout 2s df -h 2>&1 >/dev/null;then

    # pydf

    dfc -q mount -t ext4,zfs,vfat,cifs,nfs,-tmpfs -W -T

    #printf "${GRAS}${GRIS}"
    #df -ah --output=source,fstype,used,size,pcent,target --type=ext4 --type=ext3                                \
    #    --type=ext2 --type=xfs --type=nfs --type=nfs4 --type=cifs --type=fuse.sshfs                             \
    #    --type=fat --type=vfat --type=zfs | tail -n +2 | sort -r -n -k 5 |                                      \
    #    awk -v rouge=$ROUGE -v jaune=$JAUNE -v vert=$VERT -v normal=$NORMAL -v gras=$GRAS                       \
    #    'BEGIN {printf "%s %s %s %s %s %s %s %s %s %s\n", "Sys. de fichiers", "Type", "Utilisé", "Taille", gras, "Uti%", gras, "Monté sur", "Mode", normal} {uti_percent=$5 ; sub("%","",$5) ; $5=$5+0 ; cmd="/etc/profile.d/scripts/rw_ro.sh " ; cmd $1 | getline mode ; close(cmd) ; printf "%s %s %s %s %s %s %s %s %s\n", $1, $2, $3, $4, ($5>85)?rouge:($5>70?jaune:vert), uti_percent, normal, $6, mode}' | column -t


else
    echo -e $GRAS$ROUGE"Erreur lors l'exécution de la commande df : timeout"$NORMAL
fi
echo
