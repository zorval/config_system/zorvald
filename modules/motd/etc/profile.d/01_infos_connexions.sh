#!/usr/bin/env bash

############################################################
#       _  _____ _____ _____ _   _ _____ ___ ___  _   _    #
#      / \|_   _|_   _| ____| \ | |_   _|_ _/ _ \| \ | |   #
#     / _ \ | |   | | |  _| |  \| | | |  | | | | |  \| |   #
#    / ___ \| |   | | | |___| |\  | | |  | | |_| | |\  |   #
#   /_/   \_\_|   |_| |_____|_| \_| |_| |___\___/|_| \_|   #
#                                                          #
#         CE FICHIER FAIT PARTIE DU TEMPLATE ZORVAL        #
#           IL EST VERSIONNÉ DANS UN DÉPÔT GIT             #
#              MERCI DE DE PAS LE MODIFIER                 #
#                                                          #
############################################################

source /etc/profile.d/00_couleurs.sh
source /etc/zorval/env_infos_connexions

[ $bloc_system == true ] && source /etc/profile.d/01_infos_connexions.d/bloc_system.sh
[ $network == true ] && source /etc/profile.d/01_infos_connexions.d/network.sh
[ $storage == true ] && source /etc/profile.d/01_infos_connexions.d/storage.sh
[ $container == true ] && source /etc/profile.d/01_infos_connexions.d/container.sh
[ $services == true ] && source /etc/profile.d/01_infos_connexions.d/services.sh
[ $reboot  == true ] && source /etc/profile.d/01_infos_connexions.d/reboot.sh
[ $check_conf == true ] && source /etc/profile.d/01_infos_connexions.d/check_conf.sh
[ $check_log == true ] && source /etc/profile.d/01_infos_connexions.d/check_log.sh
