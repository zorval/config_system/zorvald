#!/usr/bin/env bash

############################################################
#       _  _____ _____ _____ _   _ _____ ___ ___  _   _    #
#      / \|_   _|_   _| ____| \ | |_   _|_ _/ _ \| \ | |   #
#     / _ \ | |   | | |  _| |  \| | | |  | | | | |  \| |   #
#    / ___ \| |   | | | |___| |\  | | |  | | |_| | |\  |   #
#   /_/   \_\_|   |_| |_____|_| \_| |_| |___\___/|_| \_|   #
#                                                          #
#         CE FICHIER FAIT PARTIE DU TEMPLATE ZORVAL        #
#           IL EST VERSIONNÉ DANS UN DÉPÔT GIT             #
#              MERCI DE DE PAS LE MODIFIER                 #
#                                                          #
############################################################

# Script de check des services SystemD
# Option possible : --print pour afficher sur l'écran d'accueil
# Si pas d'option : directement utilisable par NRPE

OPTION_1=$1

PRINT_MODE=false
if [ ! -z "$OPTION_1" ]; then
    if [ "$OPTION_1" = "--print" ]; then
        PRINT_MODE=true
    else
        echo "[CRITICAL] Option '$OPTION_1' inconnue"
        exit 2
    fi
fi

FIC_BASE=/etc/zorval/check_services
FIC_COLORS=/etc/profile.d/00_couleurs.sh

# Test existence et lecture des fichiers
if [ ! -f "$FIC_BASE" ]; then
    echo "Fichier $FIC_BASE manquant"
    exit 1
fi

# Récupération des couleurs (NORMAL GRAS SSLIGNE ROUGE VERT BLEU GRIS)
if [ "$PRINT_MODE" = true ]; then
    if [ -f $FIC_COLORS ]; then
        source $FIC_COLORS
    else
        echo "[CRITICAL] Impossible de lire le fichier $FIC_COLORS"
        exit 2
    fi
fi

# Fonction qui permet de tester l'état d'un service systemd passé en paramètres
# Elle attend en argument le nom du service
# Elle renvoie "$LETTER_1 $LETTER_2 $COLOR_1 $COLOR_2 $SERVICE_COLOR $SERVICE_STATUS"
test_service() {

    local SERVICE=$1

    LETTER_1=A

    IS_ENABLED=$(systemctl is-enabled "$service" 2>/dev/null)
    IS_ACTIVE=$(systemctl is-active "$service" 2>/dev/null)

    # SERVICE_STATUS=1 => problème
    # SERVICE_STATUS=0 => ok
    SERVICE_STATUS=1

    # Cas d'un service lancé par un timer, ou dépendant d'un autre service
    if [ "$IS_ENABLED" = static ]; then
        COLOR_2=GRIS
        LETTER_2=S
        if [ "$IS_ACTIVE" = inactive ]; then
            SERVICE_COLOR=NORMAL
            COLOR_1=VERT
            SERVICE_STATUS=0
        elif [ "$IS_ACTIVE" = active ]; then
            SERVICE_COLOR=NORMAL
            COLOR_1=VERT
            SERVICE_STATUS=0
        elif [ "$IS_ACTIVE" = failed ]; then
            SERVICE_COLOR=ROUGE
            COLOR_1=ROUGE
        fi

  # Cas d'un service standard, activé
  elif [ "$IS_ENABLED" = enabled ] || [ "$IS_ENABLED" = enabled-runtime ]; then
        COLOR_2=VERT
        LETTER_2=E
        # S'il est démarré
        if [ "$IS_ACTIVE" = active ]; then
            SERVICE_COLOR=NORMAL
            COLOR_1=VERT
            SERVICE_STATUS=0
        # S'il est arrêté ou en échec
        elif [ "$IS_ACTIVE" = inactive ] || [ "$IS_ACTIVE" = failed ]; then
            SERVICE_COLOR=ROUGE
            COLOR_1=ROUGE
        fi

    # Cas d'un service standard, désactivé
    elif [ "$IS_ENABLED" = disabled ]; then
        COLOR_2=ROUGE
        LETTER_2=E
        # S'il est démarré
        if [ "$IS_ACTIVE" = active ]; then
            SERVICE_COLOR=ROUGE
            COLOR_1=VERT
        # S'il est arrêté ou en échec
        elif [ "$IS_ACTIVE" = inactive ] || [ "$IS_ACTIVE" = failed ]; then
            SERVICE_COLOR=ROUGE
            COLOR_1=ROUGE
        fi

    # Cas inconnu
    else
        COLOR_1=ROUGE
        COLOR_2=ROUGE
        LETTER_2=E
        SERVICE_COLOR=ROUGE
    fi

    echo "$LETTER_1 $LETTER_2 $COLOR_1 $COLOR_2 $SERVICE_COLOR $SERVICE_STATUS"
}

# Nombre de caractère par colonne
longueur_col_1=0
longueur_col_2=0

# Mise en tableau des services
while read service;do
    if [[ "$service" =~ ^- ]];then
        service=${service#-}
         aServices=( ${aServices[@]/$service/} )
       else
         aServices+=("$service")
    fi
done <<< $(cat ${FIC_BASE}*)

#
# Première étape : pour un bel affichage, on récupère la longueur max des noms
# des services pour chaque colonne (il y a 2 colonnes)
#

if [ "$PRINT_MODE" = true ] ; then

    current_col=1
    for service in "${aServices[@]}";do

        if [ "$current_col" = 1 ] && (( ${#service} > $longueur_col_1 )); then
            longueur_col_1=${#service}
        elif [ "$current_col" = 2 ] && (( ${#service} > $longueur_col_2 )); then
            longueur_col_2=${#service}
        fi

        ((current_col++))

        if [ "$current_col" = "3" ]; then
            current_col=1
        fi

    done

fi

#
# Deuxième étape : on récupère l'état de chaque service
#

STDOUT=""
STDERR=""

if [ "$PRINT_MODE" = true ]; then
    echo -e "${GRAS}${SSLIGNE}${BLEU}Services standards$NORMAL\n"
fi

current_col=1

# Pour chaque service défini dans $FIC_BASE
for service in "${aServices[@]}";do
    read LETTER_1 LETTER_2 COLOR_1 COLOR_2 SERVICE_COLOR SERVICE_STATUS < <(test_service "$service")

    if [ "$PRINT_MODE" = true ]; then

        [ "$current_col" = 1 ] && longueur_col=$longueur_col_1 || longueur_col=$longueur_col_2
        printf "[$(eval echo \$$COLOR_1)%s${NORMAL}/$(eval echo \$$COLOR_2)%s${NORMAL}] $(eval echo \$$SERVICE_COLOR)%-${longueur_col}s${NORMAL} " "$LETTER_1" "$LETTER_2" "$service"

        ((current_col++))

        if [ "$current_col" = "3" ]; then
            echo
            current_col=1
        fi
    else
        if [ "$SERVICE_STATUS" = "0" ]; then
            STDOUT+="\n[OK] $service"
        else
            STDERR+="\n[CRITICAL] $service"
        fi
    fi
done

if [ "$PRINT_MODE" = true ] && [ "$current_col" = 2 ]; then
    echo
fi

if [ "$PRINT_MODE" = false ]; then
    RC=0
    if [ ! -z "$STDERR" ]; then
        echo -n "[CRITICAL] Services en erreur :"
        echo -e "$STDERR\n"
        RC=2
    else
        echo -n "[OK] "
    fi
    if [ ! -z "$STDOUT" ]; then
        echo -n "Services sous surveillance :"
        echo -e "$STDOUT"
    fi
    exit $RC
fi
