[![Generic badge](https://img.shields.io/badge/build-95%25-green.svg)](https://shields.io/)

[![Generic badge](https://img.shields.io/badge/code-bash-lightgrey.svg)](https://shields.io/)

[![Generic badge](https://img.shields.io/badge/Maintener-Quentin_LEJARD-lightgrey.svg)](https://framagit.org/valde/) [![Generic badge](https://img.shields.io/badge/Developer-Alban_VIDAL-lightgrey.svg)](hhttps://framagit.org/alban.vidal)

# Fonctionne sur : ![Generic badge](https://img.shields.io/badge/Debian-10-blue.svg)

## Affichage des informations utiles lors de la connexion

![](https://pic.infini.fr/UUTS8xme/3zLtR2B3)

# Configuration

## Cloner le dépôt dans /srv/git/conf/00-zorvalD

```bash
sudo mkdir -pv /srv/git/conf
# Remplacer user par l'utilisateur voulu
sudo chown user: /srv/git/conf
git clone https://git.valde.fr/zorval/zorvalD.git /srv/git/conf/00-zorvalD
```

## Execution du script

```txt
  NAME
        zorval.sh - installation de zorvalD

  SYNOPSIS
        zorval.sh [iensha]

  DESCRIPTION

        -e Installe l'environnement zorval

        -i Installe les scripts et les fichiers nécessaires aux infos de connexions

        -n Installe le paquet nftables et les fichiers dans firewall               

        -s Installe le paquet nagios-nrpe-server et les fichiers dans supervision

        -h affiche l'aide
```

> Vous pouvez combiner les options

### Infos de connexions

Le script embarque un check de vérification des dépôts qui surveille l'arborescence :

- /srv/git/conf

> Il est donc important de placer vos repos dans ce dossier (il sera plus tard possible de personnaliser l'emplacement de l'arborescence)

Vous pourrez alors avoir :

- /srv/git/conf/00-zorvalD
- /srv/git/conf/01-prod
- /srv/git/conf/02-app

Le script de vérification prendra en compte le dernier fichier du dernier dépôt et le comparera avec l'arboresence à partir de la racine.

1. renseigner l'utlisateur qui exécutera le git-auto-pull :

> par exemple si vous avez clone vos dépôts en toto et en ssh :

`vim vars`

 ```txt
 user_git_auto_pull='toto'
 ```

Enfin lancer la commande : `./zorval.sh -i`

- à l'issu:

1. Modifier le fichier **/etc/zorval/check_services** avec les services à surveiller :

```txt
ssh.service
nftables.service
```

2. Modifier si besoin **/etc/zorval/env_infos_connexions** si vous voulez enlever des blocs d'affichages

## Bonus zorval

### Nftables

- Mise en place d'un firewall logiciel (nftables)

Pour mettre en place des règles nftables uniquement, il faut utiliser l'option "-n"

`./zorval.sh -n`

Ensuite, des fichiers présents dans **/etc/nftables.d** sont disponibles pour vous aider à l'écriture des règles

### NRPE

- Mise en place de diverses check de supervision avec nrpe

Pour mettre en place du nrpe uniquement, il faut utiliser l'option "-s"
`./zorval.sh -s`

Ensuite:

- Configurer les fichiers dans /etc/nagios/nrpe.d

### Environnement zorval

- un script réalise un git pull régulier dans vos dépôts

- un script **check_conf** issu du **motd** surveille les différences entre le dépôt git cloné et l'arborescence du serveur

- Un script loggue les nouvelles connexions en SSH sur le serveur

> voir /etc/ssh/sshrc

- Un format de log clair est réécrit et ignore les logs systèmes parasites (souvent au boot) et qui pollueront votre **/var/log/check_log** si vous utiliser **motd**
