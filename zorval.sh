#!/bin/bash

################################################################################
#                 _  _____ _____ _____ _   _ _____ ___ ___  _   _              #
#                / \|_   _|_   _| ____| \ | |_   _|_ _/ _ \| \ | |             #
#               / _ \ | |   | | |  _| |  \| | | |  | | | | |  \| |             #
#              / ___ \| |   | | | |___| |\  | | |  | | |_| | |\  |             #
#             /_/   \_\_|   |_| |_____|_| \_| |_| |___\___/|_| \_|             #
#                                                                              #
#                                                                              #
#                   !! FICHIER GITTÉ - NE PAS ÉDITER ICI !!                    #
#                                                                              #
################################################################################

# TODO's
# - Ajouter support RHEL et CentOS
# - Ajouter support SLES
# - ajoutter le check des dépendences avec exit

################################################################################
# Variables
################################################################################

#set -eu

export TERMINFO='/usr/share/terminfo'
export TERM='xterm'

export cpt_cup='0'
declare -a aServices

# Path of git repository
GIT_PATH="$(realpath ${0%/*})"

# Fichier de surveillance des repos
FIC_MONITORED_REPO='/etc/zorval/git-monitored-repos'

# Paquets nécessaires au MOTD
PACKAGES='
wget
bc
monitoring-plugins-basic
git
tig
lsb-release
gawk
bsdmainutils
rsyslog
dfc
bash-completion
'

################################################################################
# Fonctions
################################################################################

function die {

  ROUGE="\033[31m"
  BLANC="\033[0m"

  >&2 echo -e "${ROUGE}Fatal: ${BLANC}${@}"
  exit 1
}
function f_display_tcup(){

    RET=$1
    cpt_cup=$2
	ROUGE="\033[31m"
    VERT="\033[32m"
    BLANC="\033[0m"
    tput bold
    tput cup ${cpt_cup} 3
    if [ ${RET} -ne 0 ]
    then
        echo -e "${ROUGE}✗${BLANC}"
		echo -e "${ROUGE}Check /tmp/config_base.log for more details${BLANC}"
		exit 100
    else
        echo -e "${VERT}✔${BLANC}"
    fi
    export cpt_cup=$((cpt_cup+1))

}
function installed {
  cmd=$(command -v "${1}")

  [[ -n "${cmd}" ]] && [[ -f "${cmd}" ]]
  return ${?}
}
function usage(){

  echo "

  $(tput bold)NAME$(tput sgr0)
                    zorval.sh - installation de zorvalD

  $(tput bold)SYNOPSIS$(tput sgr0)
                    zorval.sh [OPTION]

  $(tput bold)DESCRIPTION$(tput sgr0)

                    $(tput bold)-e$(tput sgr0) Installe l'environnement zorval

                    $(tput bold)-i$(tput sgr0) Installe les scripts et les fichiers nécessaires aux infos de connexions

                    $(tput bold)-n$(tput sgr0) Installe le paquet nftables et les fichiers dans firewall
                        
                    $(tput bold)-s$(tput sgr0) Installe le paquet nagios-nrpe-server et les fichiers dans supervision
  "
}
function install_motd(){

  eval $aServices_def

  echo -e "- [ ] [motd] - Installation des paquets si besoin pour le motd"

  if ! sudo dpkg -l $PACKAGES 2>/tmp/config_base.log 1>/dev/null
  then
      sudo apt-get -qq update &>/dev/null
      sudo apt-get -yqq install $PACKAGES &>/dev/null
  fi

  f_display_tcup $? ${cpt_cup}

  echo -e "- [ ] [motd] - Mise en place des fichiers dans /etc et /usr"
  if [ $(systemd-detect-virt) == 'lxc' ];then
    echo '
if [ -d /etc/profile.d/ ] ;then
    for profile_d_file in /etc/profile.d/*.sh
    do
        . $profile_d_file
    done
fi
' >> /root/.bashrc
  fi

  sudo \cp -rd ${GIT_PATH}/modules/motd/* /      2>/tmp/config_base.log
  f_display_tcup $? ${cpt_cup}

  echo -e "- [ ] [motd] - Création du dossier check_log"
  sudo mkdir -p /var/log/check_log 2>/tmp/config_base.log
  f_display_tcup $? ${cpt_cup}

  [[ "${aServices[@]}" =~ 'rsyslog' ]] || aServices+=("rsyslog.service")
  [[ "${aServices[@]}" =~ 'check-log' ]] || aServices+=("check-log-rotate.timer")

  export aServices_def="$(declare -p aServices)"
 
  echo -e "- [ ] [motd] - Rechargement de la configuration de systemD"
  sudo sudo systemctl daemon-reload 2>/tmp/config_base.log

  f_display_tcup $? ${cpt_cup}

}
function install_nft(){

  eval $aServices_def

  echo -e "- [ ] [nftables] - Installation du paquet nftables"

  sudo apt-get -qq update &>/dev/null
  sudo apt-get -yqq install nftables &>/dev/null

  f_display_tcup $? ${cpt_cup}

  echo -e "- [ ] [nftables] - Application des règles nftables"
  sudo \cp -rud ${GIT_PATH}/modules/firewall/* / 2>/tmp/config_base.log
  sudo nft -f /etc/nftables.conf 2>/tmp/config_base.log
  f_display_tcup $? ${cpt_cup}

  [[ "${aServices[@]}" =~ 'nftables' ]] || aServices+=("nftables.service")
 
  export aServices_def="$(declare -p aServices)"

}
function install_nrpe(){
  
  eval $aServices_def

  echo -e "- [ ] [nrpe] - Installation du paquet nagios-nrpe-server"

  sudo apt-get -qq update &>/dev/null
  sudo apt-get -yqq install nagios-nrpe-server &>/dev/null

  f_display_tcup $? ${cpt_cup}
  
  echo -e "- [ ] [nrpe] - Mise en place des fichiers de supervisions dans /etc et /usr"
  sudo cp -r ${GIT_PATH}/modules/supervision/* / 2>/tmp/config_base.log
  f_display_tcup $? ${cpt_cup}
  
  [[ "${aServices[@]}" =~ 'nagios-nrpe-server' ]] || aServices+=("nagios-nrpe-server.service")
  export aServices_def="$(declare -p aServices)"

}
function install_zorval_env(){

  eval $aServices_def

  echo -e "- [ ] [zorval_env] - Mise en place des fichiers dans /etc et /usr"
  sudo \cp -rd ${GIT_PATH}/conf/common/* /      2>/tmp/config_base.log
  f_display_tcup $? ${cpt_cup}

  echo -e "- [ ] [zorval_env] - Utilisateur qui lancera le git-auto-pull"

  source ${GIT_PATH}/vars

  if [ -n "$user_git_auto_pull" ] && ! [ "$user_git_auto_pull" == 'root' ];then
    sudo  sed -i "/User/ s/root/$user_git_auto_pull/" /etc/systemd/system/git-auto-pull.service
  fi
  f_display_tcup $? ${cpt_cup}

  [[ "${aServices[@]}" =~ 'rsyslog' ]] || aServices+=("rsyslog.service")
  [[ "${aServices[@]}" =~ 'git-auto' ]] || aServices+=("git-auto-pull.timer")
  export aServices_def="$(declare -p aServices)"

}

################################################################################
# Tests
################################################################################

touch /tmp/zorval_config.log
chmod 777 /tmp/zorval_config.log

if [[ $@ =~ --help ]] || [[ $@ =~ -h ]] || [[ $# -eq 0 ]] ;then
    usage
    exit 0
fi

installed sudo || die "Sudo absent"

if [ ! -f ${GIT_PATH}/vars ];then
    echo "user_git_auto_pull='root'" > ${GIT_PATH}/vars 2>/tmp/zorval_config.log
fi

################################################################################
# DEBUT
################################################################################

clear

while getopts "ehinsa" option; do
  case "${option}" in
    e)  install_zorval_env
        zorval_env=true  
    ;;

    i) 
        install_motd
        motd="true"
    ;;
    n)  install_nft
        nft="true"
    ;;

    s)  
        install_nrpe
        nrpe=true
        ;;

    a) 
      install_zorval_env
      install_motd
      install_nft
      install_nrpe

      zorval_env=true
      nrpe=true
      motd=true
      nft=true

    ;;

    *|h)
      usage
      exit
    ;;

  esac

done

################################################################################
# Construction du .gitignore
################################################################################

echo -e "- [ ] Construction du .gitignore"

var_gitignore=(
  '.gitignore'
  'vars'
)

printf '%s\n' "${var_gitignore[@]}" > ${GIT_PATH}/.gitignore 2>/tmp/zorval_config.log
f_display_tcup $? ${cpt_cup}

################################################################################
# - Activation des services et timers
################################################################################

cpt_cup_service=${cpt_cup}

eval "$aServices_def"

tput cup ${cpt_cup}
echo -e "- [ ] Démarrage ou redémarrage de services"
for UNIT in "${aServices[@]}";do
    if sudo systemctl is-active ${UNIT} 2>/tmp/config_base.log 1>/dev/null; then
        sudo systemctl restart ${UNIT} 2>/tmp/config_base.log 1>/dev/null
        if ! sudo systemctl is-enabled ${UNIT} 2>&1 >/dev/null ; then
	        ((cpt_cup++))
		      tput cup ${cpt_cup} 5 && echo -e "${VERT}- [✓] ${UNIT} ${BLANC} Activation au boot"
          sudo systemctl enable ${UNIT} 2>/tmp/config_base.log 1>/dev/null
        fi
    else
	    ((cpt_cup++))
        tput cup ${cpt_cup} 5 && echo -e "${VERT}- [✓] ${UNIT} ${BLANC} n'est pas démarré, on le démarre et on l'active au boot"
        sudo systemctl enable ${UNIT} 2>/tmp/config_base.log 1>/dev/null
        sudo systemctl start ${UNIT} 2>/tmp/config_base.log 1>/dev/null
    fi
done

fin_cpt_cup_service=${cpt_cup}

f_display_tcup $? ${cpt_cup_service}

cpt_cup=$((fin_cpt_cup_service+1))

################################################################################
# - Infos complémentaires
################################################################################

tput cup ${cpt_cup}

if [[ "$nrpe" == 'true' ]] || [[ "$nft" == 'true' ]] || [[ "$modt" == 'true' ]];then
    echo -e "

fichiers à modifier selon vos besoins:     

    "
fi
if [[ "$nrpe" == 'true' ]];then
    echo "[nrpe]
    - /etc/nagios/nrpe.d/allowed_hosts_pollers.cfg
    - /etc/nagios/nrpe.d/00-server-address.cfg
    "
fi
if [[ "$motd" == 'true' ]];then
    echo "[motd]
    - /etc/zorval/env_infos_connexions
    - /etc/zorval/check_services
    "
fi
if [[ "$zorval_env" == 'true' ]];then
    echo "[zorval_env]
    - /etc/zorval/env_git-auto-pull.service
    - /etc/zorval/env_ssh-logger
    "
fi
